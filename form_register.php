<?php

function isValid($date, $format = 'd/m/Y'){
    $dt = DateTime::createFromFormat($format, $date);
    return $dt && $dt->format($format) === $date;
}

$name = $gender = $faculty = $birth = $address ="";
$genders = array(0 => 'Nam', 1 => 'Nữ');
$faculties = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

$nameErr = $genderErr = $facultyErr = $birthErr = "<label></label><br>";

if (isset($_POST['submit'])) {
    
    $name = "";
    $gender = "";
    $faculty = $faculties[$_POST["faculty"]];
    $birth = "";
    $address = $_POST["address"];


    if (empty($_POST["name"]))
        $nameErr = "<label class='error'>Hãy nhập tên.</label><br>";
    else
        $name = $_POST["name"];

    if (empty($_POST["gender"]))
        $genderErr = "<label class='error'>Hãy chọn giới tính.</label><br>";
    else
        $gender =  $_POST["gender"];
    
    if (empty($_POST["birth"]))
        $birthErr = "<label class='error'>Hãy nhập ngày sinh.</label><br>";
    else if (!isValid($_POST["birth"]))
        $birthErr = "<label class='error'>Hãy nhập ngày sinh đúng định dạng.</label><br>";
    else
        $birth=  $_POST["birth"];

    if ($faculty === "")
        $facultyErr = "<label class='error'>Hãy chọn phân khoa.</label><br>";
}
?>

<head>
    <meta charset='UTF-8'>
    <link rel='stylesheet' href='form_register.css'>
</head>

<body>
    <fieldset>
        <form method='post' action='form_register.php'>
        <?php
            echo $nameErr;
            echo $genderErr;
            echo $facultyErr;
            echo $birthErr;
        ?>

            <table>

                <tr>
                    <td class='td'><label>Họ và tên<span class="star"> * </span></label></td>
                    <td>
                        <?php 
                        echo "<input type='text' id='input' class='box' name='name' value='";
                        echo isset($_POST['name']) ? $_POST['name'] : '';
                        echo "'>"; ?>
                    </td>
                        
                </tr>

                <tr>
                    <td class='td'><label>Giới tính<span class="star"> * </span></label></td>
                    <td>
                    <?php
                        for ($i = 0; $i < count($genders); $i++) {
                            echo
                                "<input type='radio' name='gender' class='gender' value='" . $genders[$i] . "'";
                            echo (isset($_POST['gender']) && $_POST['gender'] == $genders[$i]) ? " checked " : "";
                            echo "/>" . $genders[$i];
                        }
                    ?>
                    </td>
                </tr>
                
                <tr>
                    <td class='td'><label>Phân khoa<span class="star"> * </span></label></td>
                    <td>
                        <select class='box' name='faculty'>
                        <?php
                            foreach ($faculties as $key => $value) {
                                echo "<option";
                                echo (isset($_POST['faculty']) && $_POST['faculty'] == $key) ? " selected " : "";
                                echo " value='" . $key . "'>" . $value . "</option>";
                            }
                        ?>  
                        </select>
                    </td>
                </tr>

                <tr>
                    <td class='td'><label>Ngày sinh<span class="star"> * </span></label></td>
                    <td>
                        <?php 
                            echo "<input type='text' class='box' name='birth' placeholder='dd/mm/yyyy' onfocus='(this.type='date')' value='";
                            echo isset($_POST['birth']) ? $_POST['birth'] : '';
                            echo "'>"; ?>
                    </td>
                </tr>

                <tr>
                    <td class='td'><label>Địa chỉ</label></td>
                    <td>
                        <?php 
                            
                            echo "<input type='text' id='input' class='box' name='address' value='";
                            echo isset($_POST['address']) ? $_POST['address'] : '';
                            echo "'>";
                        ?>
                            
                    </td>
                </tr>

            </table>

            <button name='submit' type='submit'>Đăng ký</button>
            
        </form>

    </fieldset>

</body>

<?php
    echo "<h3>Thông tin đã nhập: </h3>";
    if (!empty($name))
        echo "Họ và tên: ", $name , "<br>";
    if (!empty($gender))
    echo "Giới tính: ", $gender , "<br>";
    if (!empty($faculty))
    echo "Phân khoa: ", $faculty , "<br>";
    if (!empty($birth))
    echo "Ngày sinh: ", $birth , "<br>";
    if (!empty($address))
    echo "Địa chỉ: ", $address , "<br>";
?>

